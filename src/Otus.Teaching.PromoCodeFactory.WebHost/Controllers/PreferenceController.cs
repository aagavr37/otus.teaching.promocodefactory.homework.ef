﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController : ControllerBase
    {
        private readonly IRepository<Preference> _preferenceRepository;

        public PreferenceController(IRepository<Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получение всех предпочтений.
        /// </summary>
        /// <returns>Список предпочтений.</returns>
        [HttpGet]
        public async Task<ActionResult<PreferenceResponce>> GetPreferenceAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            var preferencesResponce = preferences.Select(_ => new PreferenceResponce()
            {
                Id = _.Id,
                Name = _.Name
            });
            return Ok(preferencesResponce);
        }
    }
}
