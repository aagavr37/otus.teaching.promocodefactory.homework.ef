﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Employee> _employeeRepository;

        public PromocodesController(IRepository<PromoCode> promoCodeRepository, IRepository<Preference> preferenceRepository, IRepository<Customer> customerRepository, IRepository<Employee> employeeRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync();
            var promoCodesResponce = promoCodes.Select(_ => new PromoCodeShortResponse()
            {
                Id = _.Id,
                ServiceInfo = _.ServiceInfo,
                Code = _.Code,
                PartnerName = _.PartnerName,
                BeginDate = _.BeginDate.ToLongDateString(),
                EndDate = _.EndDate.ToLongDateString(),
            }).ToList();
            return Ok(promoCodesResponce);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            var preference = preferences.FirstOrDefault(_ => _.Name == request.Preference);
            if (preference == null) return BadRequest("Не найдены клиенты, у которых есть такое предпочтение.");
            var employees = await _employeeRepository.GetAllAsync();
            var employee = employees.FirstOrDefault(_ => _.FullName == request.PartnerName);
            if (employee == null) return BadRequest("Не найден партнер.");
            var customers = await _customerRepository.GetAllAsync();
            var customersPromoCodes = customers.Where(_ => _.Preferences.Any(_ => _.Preference.Id == preference.Id));
            foreach (var customer in customersPromoCodes)
            {
                var promoCode = new PromoCode()
                {
                    Id = new Guid(),
                    Code = request.PromoCode,
                    ServiceInfo = request.ServiceInfo,
                    PartnerName = request.PartnerName,
                    BeginDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(30),
                    CustomerId = customer.Id,
                    PreferenceId = preference.Id,
                    PartnerManagerId = employee.Id
                };
                await _promoCodeRepository.CreateAsync(promoCode);
                customer.PromoCodes.Add(promoCode);
                await _customerRepository.UpdateAsync(customer);
            }
            if (customersPromoCodes.Count() > 0) return Ok();
            return BadRequest("Не найдены клиенты, которым можно выдать промо-код.");
        }
    }
}